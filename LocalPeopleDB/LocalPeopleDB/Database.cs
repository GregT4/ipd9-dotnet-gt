﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalPeopleDB
{
    class Database
    {
        SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\student\Documents\ipd9-dotnet-gt\LocalPeopleDB\LocalPeopleDB\PeopleDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }
        //insert data
        public void AddPerson(Person p)
        {
            string sql = "INSERT INTO people (Name, Age) VALUES (@Name, @Age)";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = p.Name;
            cmd.Parameters.Add("@Age", SqlDbType.Int).Value = p.Age;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
        // fetch data
        public List<Person> GetAllPeople()
        {
            List<Person> result = new List<Person>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM People", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string name = (string)reader["Name"];
                    int age = (int)reader["Age"];
                    Person p = new Person(0, name, age);
                    result.Add(p);
                }
            }
             return result;
        }
    }
}
