﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentGrades
{
    class Program
    {

        static double letterToNumberGradeByArrays(string strGrade)
        {
            string[] letterList = { "A+", "A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F" };
            double[] valueList = { 4.3, 4.0, 3.7, 3.3, 3.0, 2.7, 2.3, 2.0, 1.7, 1.3, 1.0, 0.7, 0 };
            // use predicate to find index of item matching letter
            int idx = Array.FindIndex(letterList, letter => letter == strGrade);
            if (idx < 0)
            {
                throw new ArgumentOutOfRangeException("Don't know grade " + strGrade);
            }
            return valueList[idx];
        }


        // TODO: for teacher only: show how to do it with two arrays
        static double letterToNumberGrade(string strGrade)
        {
            switch (strGrade)
            {
                case "A+": return 4.3;
                case "A": return 4.0;
                case "A-": return 3.7;
                case "B+": return 3.3;
                case "B": return 3.0;
                case "B-": return 2.7;
                case "C+": return 2.3;
                case "C": return 2.0;
                case "C-": return 1.7;
                case "D+": return 1.3;
                case "D": return 1.0;
                case "D-": return 0.7;
                case "F": return 0;
                default:
                    throw new ArgumentOutOfRangeException("Don't know grade " + strGrade);
            }
        }

        static void Main(string[] args)
        {
            try
            {
                string[] lineArray = System.IO.File.ReadAllLines(@"..\..\grades.txt");
                foreach (string line in lineArray)
                {
                    string[] s1 = line.Split(':');
                    string name = s1[0];
                    string[] gradeList = s1[1].Split(',');
                    //
                    double sum = 0;
                    foreach (string strGrade in gradeList)
                    {
                        sum += letterToNumberGradeByArrays(strGrade);
                    }
                    double avg = sum / gradeList.Length;
                    Console.WriteLine("{0} has GPA {1:0.00}", name, avg);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Error reading file:  " + e.StackTrace);
            }
            Console.ReadKey();
        }
    }
}
