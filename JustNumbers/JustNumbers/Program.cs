﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNumbers
{
    class Program
    {
        static List<int> intList = new List<int>();

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Enter an integer positive number (0 to finish): ");
                string line = Console.ReadLine();
                int num;
                if (!Int32.TryParse(line, out num))
                {
                    Console.WriteLine("Invalid entry, try again");
                    continue;
                }
                if (num <= 0)
                {
                    break;
                }
                intList.Add(num);
            }
            // average
            double sum = 0;
            foreach (int n in intList)
            {
                sum += n;
            }
            double average = sum / intList.Count;
            Console.WriteLine("Average is {0}", average);
            //            
            int max = intList.Max();
            Console.WriteLine("Max is {0}", max);
            // TODO: compute median
            intList.Sort();
            double median;
            if (intList.Count % 2 == 1)
            { // odd  number - take middle
                median = intList[intList.Count / 2];
            }
            else
            { // even number - average two middle ones
                int right = intList[intList.Count / 2];
                int left = intList[intList.Count / 2 - 1];
                median = ((double)right + left) / 2;
            }
            Console.WriteLine("Median in {0}", median);
            // compute standard deviation
            double sumOfSquares = 0;
            foreach (int value in intList)
            {
                sumOfSquares += (value - average) * (value - average);
            }
            double stdDev = Math.Sqrt(sumOfSquares / intList.Count);
            Console.WriteLine("Standard deviation: {0}", stdDev);
            // output list to file
            try
            {
                StreamWriter file = new StreamWriter(@"..\..\output.txt");
                String dataLine = String.Join(";", intList);
                file.Write(dataLine);
                file.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine("FATAL ERROR: " + e.StackTrace);
            }
            //
            Console.ReadKey();
        }
    }
}
