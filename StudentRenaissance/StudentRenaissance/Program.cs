﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentRenaissance
{

    class Student
    {
        public Student(string name, double gpa)
        {
            this.Name = name;
            this.Gpa = gpa;
        }
        public string Name;
        public double Gpa;
    }

    class Program
    {
        static List<Student> StudentList = new List<Student>();

        static void LoadDataFromFile()
        {
            string[] linesArray = File.ReadAllLines(@"..\..\students.txt");
            for (int i = 0; i < linesArray.Length; i += 2)
            {
                string name = linesArray[i];
                double gpa;
                // avoid array index out of bounds exception
                if (i + 1 >= linesArray.Length)
                {
                    Console.WriteLine("Invalid file structure, odd number of lines detected");
                    continue;
                }
                if (!Double.TryParse(linesArray[i + 1], out gpa))
                {
                    Console.WriteLine("Invalid data in line {0}: {1}", i + 1, linesArray[i + 1]);
                    continue;
                }
                StudentList.Add(new Student(name, gpa));
                Console.WriteLine("GPA after is : " + gpa);
            }
        }
        static void SaveDataToFile()
        {

        }

        static int GetMenuChoice()
        {
            // show menu and get user's choice, repeat if invalid
        }

        static void Main(string[] args)
        {
            LoadDataFromFile();
            int choice;
            while ((choice = GetMenuChoice()) != 0)
            {
                switch (choice)
                {
                    case 1:
                        CreateStudent();
                        break;
                    case 2:
                        ListAllStudents();
                        break;
                    case 3:
                        ListStudentsWithNameBeginning();
                        break;
                    case 4:
                        FindAndSaveAverageGPA();
                        break;
                    default:
                        Console.WriteLine("Ooops...");
                        break;
                }
            }
            SaveDataToFile();
        }
    }
}

