﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3TodoDB
{
    class Todo
    {
        public Todo(int id,DateTime dueDate, bool isDone, string description)
        {
            Id = id;
            DueDate = dueDate;
            IsDone = isDone;
            Description = description;
        }

        public int Id {get; set;}
        public DateTime DueDate;
        public bool IsDone;
        public string Description;
    }
}
