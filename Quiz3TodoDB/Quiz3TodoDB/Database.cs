﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3TodoDB
{
    class Database
    {
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\student\Documents\ipd9-dotnet-gt\Quiz3TodoDB\Quiz3TodoDB\TodoDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }
        public void AddTodo(Todo t)
        {
            string sql = "INSERT INTO Todos (DueDate, IsDone, Description) "
                        + " VALUES (@DueDate, @IsDone,@Description)";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@DueDate", SqlDbType.Int).Value = t.DueDate;
            cmd.Parameters.Add("@IsDone", SqlDbType.NVarChar).Value = t.IsDone;
            cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = t.Description;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
        private void DeleteTodo(int id)
        {
            string sql = "DELETE FROM people WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
        private void UpdateTodo()
        {
            string sql = "UPDATE Todos SET  WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@DueDate", SqlDbType.Int).Value = b.DueDate;
            cmd.Parameters.Add("@IsDone", SqlDbType.NVarChar).Value = b.IsDone;
            cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = b.Description;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
        public List<Todo> GetAllTodos()
        {
            List<Todo> result = new List<Todo>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM Todos", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    DateTime dueDate = (DateTime)reader["DueDate"];
                    bool isDone = (bool)reader["IsDone"];
                    string description = (string)reader["Description"];
                    Todo t = new Todo(id, dueDate, isDone, description);
                    result.Add(t);
                }
            }
            return result;
        }
       
    }
}
