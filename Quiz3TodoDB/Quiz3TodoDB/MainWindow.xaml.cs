﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;

namespace Quiz3TodoDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Database db;
        public MainWindow()
        {
           try
            {
                db = new Database();
                InitializeComponent();
                RefreshTodoList();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }
        }
        private void RefreshTodoList()
        {
            lvTodoList.ItemsSource = db.GetAllTodos();
        }
        private void TbFilter_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            string filter = tbFilter.Text.ToLower();
            if (filter == "")
            {
                lvTodoList.ItemsSource = db.GetAllTodos();
            }
            else
            {
                List<Todo> list = db.GetAllTodos();
               
                var filteredList = from t in list
                                   where t.Description.ToLower().Contains(filter)
                                   select t;

                lvTodoList.ItemsSource = filteredList;
            }
        }

        private void MenuImportfromXML_Click(object sender, RoutedEventArgs e)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@"../../Todos.xml");
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                string dueDate = node["dueDate"].InnerText;
                string isDone = node["isDone"].InnerText;
                string description = node[""].InnerText;
                Console.WriteLine(dueDate, isDone, description);
            }

        }
        private void MenuFileExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        public void ButtonAddTodo_Click(object sender, RoutedEventArgs e)
        {
           return;
        }
        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
           return;
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
           return;
        }
    }
}

