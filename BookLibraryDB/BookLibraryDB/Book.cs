﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibraryDB
{
    class Book
    {
        public Book(int id, int genreId, string author, string title, string description,decimal price, DateTime pubDate)
        {
            Id = id;
            GenreId = genreId;
            Author = author;
            Title = title;
            Price = price;
            PubDate = pubDate;
            Description = description;
        }
        public int Id {get;set;}
        public int GenreId;
        public string Author {get;set;}
        public string Title {get;set;}
        public string Description;
        public decimal Price;
        public DateTime PubDate;

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6}", Id, GenreId, Author, Title, Price, PubDate, Description);
        }
    }
}
