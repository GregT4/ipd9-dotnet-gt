﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibraryDB
{
    class Database
    {
        SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\student\Documents\ipd9-dotnet-gt\BookLibraryDB\BookLibraryDB\BookLibraryDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }
        public void AddBook(Book b)
        {
            string sql = "INSERT INTO books (GenreId, Author, Title, Price, PubDate, Description) "
                        + " VALUES (@GenreId, @Author, @Title, @Price, @PubDate, @Description)";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@GenreId", SqlDbType.Int).Value = b.GenreId;
            cmd.Parameters.Add("@Author", SqlDbType.NVarChar).Value = b.Author;
            cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = b.Title;
            cmd.Parameters.Add("@Price", SqlDbType.Money).Value = b.Price;
            cmd.Parameters.Add("@PubDate", SqlDbType.Date).Value = b.PubDate;
            cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = b.Description;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
        public List<Book> GetAllBooks()
        {
            List<Book> result = new List<Book>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM Books", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    int genreId = (int)reader["GenreId"];
                    string author = (string)reader["Author"];
                    string title = (string)reader["Title"];
                    decimal price = (decimal)reader["Price"];
                    DateTime pubDate = (DateTime)reader["PubDate"];
                    string description = (string)reader["Description"];
                    Book b = new Book(id, genreId, author, title, description, price, pubDate);
                    result.Add(b);
                }
            }
            return result;
        }
        public List<Genre> GetAllGenres()
        {
            List<Genre> result = new List<Genre>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM Genres", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string name = (string)reader["Name"];
                    Genre g = new Genre(id, name);
                    result.Add(g);
                }
            }
            return result;
        }
        internal void UpdateBook(Book p)
        {
            /*            string sql = "UPDATE people SET Name = @Name, Age = @Age WHERE Id=@Id";
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.Parameters.Add("@Id", SqlDbType.Int).Value = p.Id;
                        cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = p.Name;
                        cmd.Parameters.Add("@Age", SqlDbType.Int).Value = p.Age;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery(); */
        }
        internal void DeleteBookById(int id)
        {
                        string sql = "DELETE FROM people WHERE Id=@Id";
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery(); 
        }
    }
}
