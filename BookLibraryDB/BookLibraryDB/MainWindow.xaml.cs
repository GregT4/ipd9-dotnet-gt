﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace BookLibraryDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Database db;
        public MainWindow()
        {
            try
            {
                db = new Database();
                InitializeComponent();
                RefreshBookList();
                // TODO: load genres into combo box

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }
        }
        private void RefreshBookList()
        {
            lvBookList.ItemsSource = db.GetAllBooks();
        }
        private void TbFilter_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            string filter = tbFilter.Text.ToLower();
            if (filter == "")
            {
                lvBookList.ItemsSource = db.GetAllBooks();
            }
            else
            {
                List<Book> list = db.GetAllBooks();
                /* var filteredList = list.Where(b => b.Title.ToLower().Contains(filter)
                                                   || b.Author.ToLower().Contains(filter)); */
                var filteredList = from b in list
                    where b.Title.ToLower().Contains(filter) || b.Author.ToLower().Contains(filter)
                                       select b;

                lvBookList.ItemsSource = filteredList;
            }
        }
        private void MenuImportfromXML_Click(object sender, RoutedEventArgs e)
        {
            var doc = XDocument.Load(new StreamReader(@"../../books.xml"));
            
        }
            private void MenuFileExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
