﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Quiz1BankingRecords
{
    public class AccountTransaction
    {
        public readonly DateTime date;
        public readonly string description;
        public readonly decimal deposit;
        public readonly decimal withdrawal;

        public AccountTransaction(String line)
        {
            string[] lineContent = line.Split(';');
            for (int i = 0; i < lineContent.Length; ++i)
            {
                switch (i)
                {
                    case 0:
                        try
                        {
                            this.date = DateTime.Parse(lineContent[i]);
                            break;
                        }
                        catch (FormatException e)
                        {
                            throw new FileFormatException();
                        }

                    case 1:
                        if (lineContent[i].Length < 2 || String.IsNullOrEmpty(lineContent[i]))
                        {
                            throw new FileFormatException();
                        }
                        this.description = lineContent[i];
                        break;
                    case 2:
                        if (decimal.Parse(lineContent[i]) < 0)
                        {
                            throw new FileFormatException();
                        }
                        else
                        {
                            if ((decimal.Parse(lineContent[i]) == 0) && (decimal.Parse(lineContent[i + 1]) == 0))
                            {
                                throw new FileFormatException();
                            }
                        }
                        this.deposit = decimal.Parse(lineContent[i]);
                        break;
                    case 3:
                        if (decimal.Parse(lineContent[i]) < 0)
                        {
                            throw new FileFormatException();
                        }
                        this.withdrawal = decimal.Parse(lineContent[i]);
                        break;
                    default:
                        throw new FileFormatException();

                }
            }
        }
    }
    class Program
    {
        static List<AccountTransaction> transactionList = new List<AccountTransaction>();
        static void Main(string[] args)
        {
            int errorCount = 0;
            decimal sumOfDepasit = 0, sumOfWithdrawal = 0;
            try
            {
                string[] lineArray = System.IO.File.ReadAllLines(@"..\..\operations.txt");
                foreach (string line in lineArray)
                {
                    try
                    {
                        transactionList.Add(new AccountTransaction(line));
                    }
                    catch (FileFormatException)
                    {
                        errorCount++;
                        continue;
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Error reading file:  " + e.StackTrace);
            }

            foreach (AccountTransaction t in transactionList)
            {
                sumOfDepasit += t.deposit;
                sumOfWithdrawal += t.withdrawal;
            }

            Console.WriteLine("There is {0} Invalid Data Line in file.", errorCount);
            Console.WriteLine("Final balance of the account is: {0}", sumOfDepasit - sumOfWithdrawal);
            Console.ReadKey();

        }
    }
}
