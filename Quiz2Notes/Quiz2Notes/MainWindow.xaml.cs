﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2Notes
{
    class Note
    {
        public Note(string title, string body)
        {
            this.Title = title;
            this.Body = body;
            _id = ++instanceCount;
        }

        private static int instanceCount;
        private int _id;
        public int Id
        {
            get
            {
                return _id;
            }
        }

        private string _title;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentException("Title must be 2-100 characters long");
                }
                _title = value;
            }
        }

        private string _body;
        public string Body
        {
            get
            {
                return _body;
            }
            set
            {
                if (value.Length < 2 || value.Length > 2000)
                {
                    throw new ArgumentException("Notes must be 2-2000 characters long");
                }
                _body = value;
            }
        }

    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Note> NotesList = new List<Note>();

        private bool unsavedChanges = false;
        private string openFilePath = null;

        public MainWindow()
        {
            InitializeComponent();
            lvNotes.ItemsSource = NotesList;
            updateStatus();
        }

        private void MenuFileOpen_Click(object sender, RoutedEventArgs e)
        {
            if (unsavedChanges)
            {
                MessageBoxResult result = MessageBox.Show("Save unsaved changes?", "Unsaved changes",
                    MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                switch (result)
                {
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        return;
                    case MessageBoxResult.Yes:
                        if (openFilePath == null)
                        {
                            MenuFileSaveAs_Click(null, null);
                        }
                        else
                        {
                            MenuFileSave_Click(null, null);
                        }
                        break;
                }
            }
            //
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Text files (*.notes)|*.notes|All files (*.*)|*.*";
                if (openFileDialog.ShowDialog() == true)
                {
                    openFilePath = openFileDialog.FileName;
                    tbBody.Text = File.ReadAllText(openFilePath);
                    unsavedChanges = false;
                    updateStatus();
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show(this, "Error reading file " + ex.Message);
            }
        }
        private void MenuFileSave_Click(object sender, RoutedEventArgs e)
        {
            if (openFilePath == null)
            {
                MenuFileSaveAs_Click(null, null);
            }
            else
            {
                try
                {
                    File.WriteAllText(openFilePath, tbBody.Text);
                    unsavedChanges = false;
                    updateStatus();
                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex.StackTrace);
                    MessageBox.Show(this, "Error reading file " + ex.Message);
                }

            }
        }
        private void MenuFileSaveAs_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text files (*.notes)|*.notes|All files (*.*)|*.*";
                if (saveFileDialog.ShowDialog() == true)
                {
                    openFilePath = saveFileDialog.FileName;
                    File.WriteAllText(openFilePath, tbBody.Text);
                    unsavedChanges = false;
                    updateStatus();
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show(this, "Error writing file " + ex.Message);
            }
        }
        private void MenuFileExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void updateStatus()
        {
            Title = (openFilePath == null ? "new file" : System.IO.Path.GetFileName(openFilePath))
                + (unsavedChanges ? " (unsaved changes)" : "");
            lblOpenFilePath.Text = openFilePath == null ? "new file" : openFilePath;
        }

        private void tbNotes_TextChanged(object sender, TextChangedEventArgs e)
        {
            unsavedChanges = true;
            updateStatus();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (unsavedChanges)
            {
                MessageBoxResult result = MessageBox.Show("Save unsaved changes?", "Unsaved changes",
                    MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                switch (result)
                {
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                    case MessageBoxResult.Yes:
                        if (openFilePath == null)
                        {
                            // FIXME: should not cancel, unless SaveAs was cancelled
                            e.Cancel = true;
                            MenuFileSaveAs_Click(null, null);
                        }
                        else
                        {
                            MenuFileSave_Click(null, null);
                        }
                        break;
                }
            }
        }
        private void SaveTitle_onLostFocus(object sender, SelectionChangedEventArgs e)
        {
            int index = lvNotes.SelectedIndex;
            if (index < 0)
            {
                 return;
            }
            Note n = NotesList[index];
            tbTitle.Text = n.Title;
            tbBody.Text = n.Body;
        }

        private void SaveBody_onLostFocus(object sender, SelectionChangedEventArgs e)
        {
            int index = lvNotes.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Note n = NotesList[index];
            tbTitle.Text = n.Title;
            tbBody.Text = n.Body;
        }
        private void MenueNotesCreateNew_Click(object sender, RoutedEventArgs e)
        {
            string title = tbTitle.Text;
            string body = tbBody.Text;

            Note n = new Note(title, body);
            NotesList.Add(n);
            lvNotes.Items.Refresh();
        }
        private void MenueNotesDeleteSelected_Click(object sender, RoutedEventArgs e)
        {
            int index = lvNotes.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            NotesList.RemoveAt(index);
            lvNotes.Items.Refresh();
        }
    }
}
